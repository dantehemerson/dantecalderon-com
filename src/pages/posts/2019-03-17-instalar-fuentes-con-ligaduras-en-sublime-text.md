---
model: post
title: Instalar fuentes con ligaduras en Sublime Text
description: Instalar fuentes con ligaduras en Sublime Text.
path: install-font-with-ligatures-sublime-text
thumbnail: /img/screenshot_2019-03-17_11-15-36.png
date: 2019-03-17T15:03:27.829Z
published: true
style: doble-column
tags:
  - SublimeText
  - Fonts
---

Las fuentes
